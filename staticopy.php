<?php declare(strict_types=1);
/**
 *  Staticopy can be used to transform a dynamic website into a static website.
 *  
 *  It needs a list of URLs with HTML and/or a list of sitemap.xml URLs. The HTML is stored in filesystem to be served with rewrite rules of a webserver.
 *  
 * @author     Carsten Logemann <c-logemann.dev@nodegard.com>
 * @copyright  2019 Nodegard GmbH
 * @license    GPLv2
 * @version    0.1
 * @link       https://gitlab.com/staticopy/staticopy
 *  
 *  @parameter argv[1] (json_array)
 *  
 *  TODO some type of explanation of input values
 *
 */
set_error_handler("staticopyErrorHandler");
$allPaths = ['/' => '/'];
$httpErrors = $fileErrors = [];
 
if (!isset($argv[1])) {
    trigger_error("Missing argument for json file or json data");
}
$set = getParameterSet($argv[1]);

validateParameterSet($set);

foreach ($set['maps'] as $mapPath) {
    list($validMapPaths, $httpErrors[], $httpResponse) = getValidPathsFromMap(
        $mapPath,
        $set['baseurl'],
        $set['header'],
        $set['agent'],
        $set['login']
        );
    
    $allPaths = array_merge($allPaths, $validMapPaths);
    
    $fileErrors[] = writeStaticMap($mapPath, $httpResponse, $set['basefolder']);
}

foreach ($set['paths'] as $value) {
    $path = addLeadingSlash($value);
    $allPaths[$path] = $path;
}

foreach ($allPaths as $value) {
    list($httpErrors[], $httpResponse) = getContentFromPath(
        $value,
	      $set['baseurl'],
        $set['header'],
        $set['agent'],
        $set['login']
        );
    
    $fileErrors[] = writeStaticContent(
        $value,
        $set['baseurl'],
        $set['basefolder'],
        $httpResponse
        );
}

exit(json_encode(['error' => [
            'request' => $httpErrors,
            'file' => $fileErrors
            ]
        ]));
// End of main process.

/**
 * Collect all the paths of a given xml map structure e.g sitemap.xml
 * 
 * @param string $mapPath
 * @param string $baseUrl
 * @param array $header
 * @param string $agent
 * @param string $credential
 * @return array
 */
function getValidPathsFromMap(
  string $mapPath, 
  string $baseUrl,
  array $header, 
  string $agent,
  string $credential
  ) : array
{
    $httpError = $validPaths = [];
    $xmlSiteMapUrl = $baseUrl . addLeadingSlash($mapPath);
    //check if map can be requested via http
    $response = requestUrl($xmlSiteMapUrl, $header, $agent, $credential);
    
    if ($response['http_code'] !== 200) {
        $httpError[$response['http_code']][] = $mapPath;
    } else {
        $validPaths = createPathArray($response['content']);
    }
    
    return [$validPaths, $httpError, $response['content']];
}

/**
 * Creates a Paths from given
 * @param string $httpResponse
 * @return array
 */
function createPathArray(string $httpResponse) : array
{
    $pathAr = [];
    $xml_object = simplexml_load_string($httpResponse);
    
    foreach ($xml_object->url as $mapurl) {
        $loc = (string) $mapurl->loc;
        $loc_parts = parse_url($loc);
        $loc_baseurl = $loc_parts['scheme'] . "://" . $loc_parts['host'];
        $path = str_replace($loc_baseurl, "", $loc);
        $pathAr[$path] = $path;
    }
    
    return $pathAr;
}

/**
 * Write Map to file (staticopy sitemap itself)
 * 
 * @param string $mapPath
 * @param string $content
 * @param string $basefolder
 * @return string
 */
function writeStaticMap(
  string $mapPath, 
  string $content, 
  string $basefolder
  ) : string
{
    $fileError = "";
    $mapPathParts = pathinfo($mapPath);
    
    if ( !doWrite($basefolder . "/" . $mapPathParts['dirname'], $mapPathParts['basename'] . '_.html', $content)) {
        $fileError = $mapPath;
    }
    
    return $fileError;
}

/**
 * If Path is reacheable via http request
 * return http response containing the rendered page
 * 
 * @param string $path
 * @param string $baseUrl
 * @param array $header
 * @param string $agent
 * @param string $credential
 * @return array
 */
function getContentFromPath(
    string $path,
    string $baseUrl,
    array $header,
    string $agent,
    string $credential
    ) : array
{
    $httpError = [];
    $crawlUrl = $baseUrl . $path;
    
    $response = requestUrl($crawlUrl, $header, $agent, $credential);
    
    if ($response['http_code'] !== 200) {
        $httpError[$response['http_code']][] = $path;
    }
    
    return [$httpError, $response];
}
/**
 * Write http Content to file maintaining folder structure of given map
 * 
 * @param string $path
 * @param string $baseUrl
 * @param string $baseFolder
 * @param array $response
 * @return string
 */
function writeStaticContent(
    string $path,
    string $baseUrl,
    string $baseFolder,
    array $response
    ) : string
{
    $fileError = "";
    $crawlUrl = $baseUrl . $path;
    
    $urlParts = parse_url($crawlUrl);
    $query = '';
    
    if (isset($urlParts['query'])) {
        $query = $urlParts['query'];
    }
        
    $pathParts = pathinfo($urlParts['path']);
    $directory = $baseFolder. "/". $pathParts['dirname'];
    $fileCache = $pathParts['basename'] . "_" . $query . ".html";
    
    //TODO optional:
    $content = str_replace($baseUrl, "", $response['content']);
    
    if (!doWrite($directory, $fileCache , $content)) {
        $fileError = $path;
    }
    
   return $fileError;
}

/**
 * Manages and places repeated http requests
 *
 * @param string $url
 * @param array $header
 * @param string $agent
 * @param string $credential
 * @return ['header','body', 'curl_error','http_code','last_url' ]
 */
function requestUrl(
  string $url, 
  array $header, 
  string $agent, 
  string $credential)
  : array
{
    $try = 1;
    while ($try < 5) {
        $response = doCurlRequest($url, $header, $agent, $credential);
        if (($response['http_code'] === 200) || ($response['http_code'] === 404)) {
            return $response;
        }
        
        usleep(250000 * $try);
        $try++;
    }
    return $response;
}

/**
 * Request a cURL HTTP Reponse for given url and request parameters 
 * 
 * @param string $url
 * @param array $header
 * @param string $agent
 * @param string $credential
 * @return array
 */
function doCurlRequest(
  string $url, 
  array $header, 
  string $agent, 
  string $credential)
  : array
{
    $result = [
        'header' => '',
        'body' => '',
        'curl_error' => '',
        'http_code' => '',
        'last_url' => '',
        'content' => ''
    ];
    
    if (!$ch = curl_init($url)) {
        trigger_error("No Curl Handle present for $url", E_USER_ERROR);
    }
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
    curl_setopt($ch, CURLOPT_USERPWD, $credential );

    $response = curl_exec($ch);
    $error = curl_error($ch);

    if ($error !== "") {
        trigger_error("Curl Error: $error", E_USER_ERROR);
    }
    
    $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $headerSize);
    $result['body'] = substr($response, $headerSize );
    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $result['content'] = $response;
    curl_close ($ch);

    return $result;
}

 /**
 * Helper function to store files.
 * 
 * @param string $basepath
 * @param string $filename
 * @param string $content
 * @return bool
 */
function doWrite(
  string $basepath, 
  string $filename, 
  string $content
  ) : bool
{
  if (!file_exists($basepath)) {
    mkdir($basepath, 0750, true);
  }
  $path = $basepath . "/" . $filename;
  
  if (file_put_contents($path, $content)) {
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Add slash at the beginning of given path
 *
 * @param string $path
 * @return string
 */
function addLeadingSlash(string $path) : string
{
    $pos = strpos($path, '/');
    if (($pos === false) || ($pos != 0)) {
        return '/' . $path;
    }
    return $path;
}

/**
 * Turns json string or file into array
 *
 * @param string $json
 * @return array
 */
function getParameterSet(string $json) : array
{
    $set = json_decode($json, true);
    if (is_array($set)) {
        return $set;
    }
    
    if (file_exists($json)) {
        $set = json_decode(file_get_contents($json), true);
    }
    
    if (!is_array($set) || 1 > count($set)) {
        trigger_error("Json Input String invalid. Could not be converted to array", E_USER_ERROR);
    }
    
    return $set;
}

/**
 * Check if all necessary inputs were given 
 * 
 * @param array $set
 */
function validateParameterSet(array $set) : void
{
    if (!isset($set['maps']) || !is_array($set['maps'])) {
        trigger_error("Parameter 'maps' either missing or malformed", E_USER_ERROR);
    }
    
    if (!isset($set['baseurl']) || !is_string($set['baseurl'])) {//TODO check valid url with http/https
        trigger_error("Parameter 'baseurl' either missing or malformed", E_USER_ERROR);
    }
    
    if (!isset($set['paths']) || !is_array($set['paths'])) {
        trigger_error("Parameter 'paths' either missing or malformed", E_USER_ERROR);
    }
    
    if (!isset($set['login']) || !is_string($set['login'])) {//TODO check str:str
        trigger_error("Parameter 'login' either missing or malformed", E_USER_ERROR);
    }
        
    if (!isset($set['basefolder']) || !is_string($set['basefolder'])) {//TODO ceck valid url
        trigger_error("Parameter 'basefolder' either missing or malformed", E_USER_ERROR);
    }
    
    if (!isset($set['agent']) || !is_string($set['agent'])) {//TODO check if valid agent
        trigger_error("Parameter 'agent' either missing or malformed", E_USER_ERROR);
    }
    
    if (!isset($set['header']) || !is_array($set['header'])) {
        trigger_error("Parameter 'paths' either missing or malformed", E_USER_ERROR); //TODO valid header
    }
}

/**
 * Staticopy Error Handler to echo error and quit the script
 */
function staticopyErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting, so let it fall
        // through to the standard PHP error handler
        return false;
    }
    switch ($errno) {
        case E_USER_ERROR:
            echo "[$errno] $errstr\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n";
            echo "Aborting...\n";
            exit(1);
            break;
            
        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
            break;
            
        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
            break;
            
        default:
            echo "Unknown error type: [$errno] $errstr<br />\n";
            break;
    }
    /* Don't execute PHP internal error handler */
    return true;
}